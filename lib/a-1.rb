# Given an array of unique integers ordered from least to greatest, write a
# method that returns an array of the integers that are needed to
# fill in the consecutive set.

def missing_numbers(nums)

  nums.sort
  x = nums[0]
  y = nums[-1]
  (x..y).to_a - nums

end

# Write a method that given a string representation of a binary number will
# return that binary number in base 10.
#
# To convert from binary to base 10, we take the sum of each digit multiplied by
# two raised to the power of its index. For example:
#   1001 = [ 1 * 2^3 ] + [ 0 * 2^2 ] + [ 0 * 2^1 ] + [ 1 * 2^0 ] = 9
#
# You may NOT use the Ruby String class's built in base conversion method.

def base2to10(binary)
  chars = binary.split("").map(&:to_i)
   result = []
   chars.reverse.map.with_index do |ch,idx|
     result << (ch * (2**idx))
   end
   result.reduce(:+)
end

class Hash

  # Hash#select passes each key-value pair of a hash to the block (the proc
  # accepts two arguments: a key and a value). Key-value pairs that return true
  # when passed to the block are added to a new hash. Key-value pairs that return
  # false are not. Hash#select then returns the new hash.
  #
  # Write your own Hash#select method by monkey patching the Hash class. Your
  # Hash#my_select method should have the functionailty of Hash#select described
  # above. Do not use Hash#select in your method.

  def my_select(&prc)
    result = {}
         self.each do |key, value|
             if yield(key,value)
               result[key] = value
             end
         end
         result
  end


end

class Hash

  # Hash#merge takes a proc that accepts three arguments: a key and the two
  # corresponding values in the hashes being merged. Hash#merge then sets that
  # key to the return value of the proc in a new hash. If no proc is given,
  # Hash#merge simply merges the two hashes.
  #
  # Write a method with the functionality of Hash#merge. Your Hash#my_merge method
  # should optionally take a proc as an argument and return a new hash. If a proc
  # is not given, your method should provide default merging behavior. Do not use
  # Hash#merge in your method.

  def my_merge(hash, &prc)
    unless block_given?
      self.each{ |k, value| hash[k] = value unless hash.keys.include?(k)}
      return hash
    end
    result = {}
    hash.each do |k, value|
      if self[k] != nil
        new_val = prc.call(k, self[k], value)
        result[k] = new_val
      else
        result[k] = value
      end
    end
    self.each do |k, value|
      result[k] = value if result[k] == nil
    end
    result
  end

end

# The Lucas series is a sequence of integers that extends infinitely in both
# positive and negative directions.
#
# The first two numbers in the Lucas series are 2 and 1. A Lucas number can
# be calculated as the sum of the previous two numbers in the sequence.
# A Lucas number can also be calculated as the difference between the next
# two numbers in the sequence.
#
# All numbers in the Lucas series are indexed. The number 2 is
# located at index 0. The number 1 is located at index 1, and the number -1 is
# located at index -1. You might find the chart below helpful:
#
# Lucas series: ...-11,  7,  -4,  3,  -1,  2,  1,  3,  4,  7,  11...
# Indices:      ... -5, -4,  -3, -2,  -1,  0,  1,  2,  3,  4,  5...
#
# Write a method that takes an input N and returns the number at the Nth index
# position of the Lucas series.

def lucas_numbers(n)
  result = [2, 1]
  (n.abs-1).times do
    result.push result.slice(-2..-1).reduce(:+)
  end
  n >= 0 ? result[n] : ( n.even? ? result[n.abs] : -result[n.abs])
end

# A palindrome is a word or sequence of words that reads the same backwards as
# forwards. Write a method that returns the longest palindrome in a given
# string. If there is no palindrome longer than two letters, return false.

def palindrome?(str)
  str == str.reverse
end

def longest_palindrome(string)
  words = string.split('')
  best = ''
  result = []

  idx1 = 0
  while idx1 < words.length
    best = ''
    ele1 = words[idx1]
    best << ele1
    idx2 = idx1 + 1

    while idx2 < words.length
      ele2 = words[idx2]
      best << ele2
      best.split.each do |word|
        if palindrome?(word)
          result << word
        end
      end
      idx2 += 1
    end
    idx1 += 1
  end
  answer = result.sort_by {|x| x.length}.last
  if answer == nil
    return false
  else
    answer.length
  end
  end
